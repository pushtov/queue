<?php

use yii\db\Migration;

class m170620_134608_create_product_tables extends Migration
{
    public function safeUp()
    {
        // Таблица с продуктами
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        // Таблица с торговыми предложениями
        $this->createTable('{{%trade_offer}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->decimal(10, 2),
            'product_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'fk-trade_offer-product',
            '{{%trade_offer}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        // Таблица с модификациями продукта
        $this->createTable('{{%modification}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'product_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'fk-modification-product',
            '{{%modification}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-modification-product', '{{%modification}}');
        $this->dropTable('{{%modification}}');
        $this->dropForeignKey('fk-trade_offer-product', '{{%trade_offer}}');
        $this->dropTable('{{%trade_offer}}');
        $this->dropTable('{{%product}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170620_134608_create_product_tables cannot be reverted.\n";

        return false;
    }
    */
}

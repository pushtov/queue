<?php
/**
 * @var $this yii\web\View
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use yii\bootstrap\Html;
use yii\widgets\ListView;

$this->title = 'Выбор модификации';
?>
<div class="b-select-trade-offer">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-12">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_touch-button',
                'summary' => false,
                'options' => [
                    'class' => 'b-touch-buttons row',
                ],
                'itemOptions' => [
                    'class' => 'b-touch-buttons__btn-wrap col-lg-3 col-sm-4 col-xs-6',
                ],
            ]) ?>
        </div>
    </div>
</div>
<?php

namespace app\modules\shop\controllers;

use app\models\Product;
use Yii;
use app\models\TradeOffer;
use yii\data\ActiveDataProvider;
use app\modules\shop\components\ShopController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TradeOfferController implements the CRUD actions for TradeOffer model.
 */
class TradeOfferController extends ShopController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Список торговых предложений продукта
     *
     * @param integer $product_id Идентификатор продукта
     * @return mixed
     */
    public function actionIndex($product_id)
    {
        $product = Product::findOne($product_id);
        if ($product === null) {
            return $this->goHomeWithError('Некорректный запрос');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $product->getTradeOffers(),
        ]);

        return $this->render('index', [
            'product' => $product,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TradeOffer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TradeOffer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $product_id
     * @return mixed
     */
    public function actionCreate($product_id)
    {
        $product = Product::findOne($product_id);
        if ($product === null) {
            return $this->goHomeWithError('Ошибка запроса');
        }
        $model = new TradeOffer();
        $model->product_id = $product->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'product' => $product,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TradeOffer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TradeOffer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TradeOffer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return TradeOffer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TradeOffer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не существует');
        }
    }
}

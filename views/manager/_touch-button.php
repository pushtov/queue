<?php
/**
 * @var \app\models\ActionButtonInterface $model
 */

use yii\bootstrap\Html;
?>
<?= Html::a($model->buttonName, $model->actionUrl, ['class' => 'b-touch-button b-touch-buttons__btn']) ?>
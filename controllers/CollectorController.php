<?php

namespace app\controllers;

use app\libs\enums\OrderEvent;
use app\models\Order;
use izumi\longpoll\Event;
use izumi\longpoll\LongPollAction;
use izumi\longpoll\Server;
use yii\web\Controller;

class CollectorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->view->params['section_name'] = 'Панель сборщика';
        $this->view->params['home_url'] = ['/collector'];
        $this->view->params['header_menu'] = [
            ['label' => 'Панель менеджера', 'url' => ['/manager']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'polling' => [
                'class' => LongPollAction::className(),
                'events' => array_keys(OrderEvent::getConstantsByValue()),
                'callback' => [$this, 'longPollCallback'],
            ],
        ];
    }

    /**
     * Метод для генерации ответа в long poll
     *
     * @param Server $server
     */
    public function longPollCallback(Server $server)
    {
        $response = [];
        foreach ($server->triggeredEvents as $event) {
            /** @var Event $event */
            $response[$event->key] = OrderEvent::createByValue($event->key)->getResult($event);
        }
        $server->responseData = $response;
    }

    public function actionIndex()
    {
        $currentOrders = Order::find()->where(['status' => Order::STATUS_COMPLETED])->all();
        return $this->render('index', [
            'currentOrders' => $currentOrders,
        ]);
    }

}

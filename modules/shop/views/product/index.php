<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = 'Продукты';
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать продукт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'options' => ['class' => 'b-touch-buttons row'],
        'itemOptions' => ['class' => 'b-touch-buttons__btn-wrap col-lg-3 col-sm-4 col-xs-6'],
        'itemView' => function ($model) {
            return Html::a(Html::encode($model->name), ['view', 'id' => $model->id], [
                'class' => 'b-touch-button b-touch-buttons__btn',
            ]);
        },
    ]) ?>
    <?php Pjax::end(); ?></div>

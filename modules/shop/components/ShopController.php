<?php

namespace app\modules\shop\components;

use Yii;
use yii\web\Controller;

class ShopController extends Controller
{
    /**
     * URL корневого раздела модуля
     *
     * @var array
     */
    public $homeUrl = ['/site'];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->view->params['section_name'] = 'Управление ассортиментом';
        $this->view->params['home_url'] = ['/shop'];
        $this->view->params['header_menu'] = [
            ['label' => 'Панель менеджера', 'url' => ['/manager']],
            ['label' => 'Панель сборщика', 'url' => ['/collector']],
        ];

        $this->view->params['breadcrumbs'][] = ['label' => 'Управление', 'url' => ['/shop']];
    }

    /**
     * Редирект с сообщением об ошибке
     *
     * @param array|string $url
     * @param string|bool $message
     * @param integer $statusCode
     * @return \yii\web\Response
     */
    public function redirectWithError($url, $message = true, $statusCode = 302)
    {
        Yii::$app->session->setFlash('error', $message);
        return $this->redirect($url, $statusCode);
    }

    /**
     * Редирект с сообщением об успешной операции
     *
     * @param array|string $url
     * @param string|bool $message
     * @param integer $statusCode
     * @return \yii\web\Response
     */
    public function redirectWithSuccess($url, $message = true, $statusCode = 302)
    {
        Yii::$app->session->setFlash('success', $message);
        return $this->redirect($url, $statusCode);
    }

    /**
     * Редирект на главную страницу модуля с сообщением об ошибке
     *
     * @param bool $message
     * @return \yii\web\Response
     */
    public function goHomeWithError($message = true)
    {
        return $this->redirectWithError($this->homeUrl, $message);
    }

    /**
     * Редирект на главную страницу модуля с сообщением об успешной операции
     *
     * @param bool $message
     * @return \yii\web\Response
     */
    public function goHomeWithSuccess($message = true)
    {
        return $this->redirectWithSuccess($this->homeUrl, $message);
    }
}
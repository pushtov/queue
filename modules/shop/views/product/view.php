<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = Html::encode($model->name);

?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="b-btn-toolbar btn-toolbar" role="toolbar">
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить продукт?',
                'method' => 'post',
            ],
        ]) ?>
        <div class="btn-group" role="group">
            <?= Html::a('Торговые предложения', ['/shop/trade-offer', 'product_id' => $model->id], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Модификации', ['/shop/modification', 'product_id' => $model->id], ['class' => 'btn btn-default']) ?>
        </div><!-- .btn-group -->
    </div><!-- .btn-toolbar -->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'category_id',
                'value' => $model->category->name,
            ],
        ],
    ]) ?>

</div>

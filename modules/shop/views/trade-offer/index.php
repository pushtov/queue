<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $product \app\models\Product */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Торговые предложения';
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($product->name), 'url' => ['/shop/product/view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = 'Торговые предложения';
?>
<div class="trade-offer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать торговое предложение', ['create', 'product_id' => $product->id], ['class' => 'btn btn-success']); ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'options' => ['class' => 'b-touch-buttons row'],
        'itemOptions' => ['class' => 'b-touch-buttons__btn-wrap col-lg-3 col-sm-4 col-xs-6'],
        'itemView' => function ($model) {
            return Html::a(Html::encode($model->name), ['view', 'id' => $model->id], [
                'class' => 'b-touch-button b-touch-buttons__btn',
            ]);
        },
    ]) ?>
    <?php Pjax::end(); ?></div>

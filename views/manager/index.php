<?php
/**
 * @var $this yii\web\View
 * @var \yii\data\ActiveDataProvider $menuDataProvider
 * @var \app\libs\data\SessionDataProvider $newOrderDataProvider
 * @var \yii\data\ActiveDataProvider $ordersDataProvider
 */

use app\libs\widgets\NewOrder;
use yii\bootstrap\Alert;
use yii\bootstrap\Html;
use yii\widgets\ListView;

$this->title = 'Панель менеджера';
?>
<div class="b-manager-panel full-height">
    <div class="row full-height">
        <div class="col-md-8 full-height overflow-auto">
            <h1><?= Html::encode($this->title) ?></h1>
            <?php if (($error = Yii::$app->session->getFlash('error')) !== null): ?>
                <?= Alert::widget([
                    'options' => [
                        'class' => 'alert-danger',
                    ],
                    'body' => $error,
                ]) ?>
            <?php endif ?>
            <?php if (($success = Yii::$app->session->getFlash('success')) !== null): ?>
                <?= Alert::widget([
                    'options' => [
                        'class' => 'alert-success',
                    ],
                    'body' => $success,
                ]) ?>
            <?php endif ?>
            <?php if ($newOrderDataProvider->count > 0): ?>
                <?= NewOrder::widget(['dataProvider' => $newOrderDataProvider]) ?>
            <?php endif ?>
            <?= ListView::widget([
                'dataProvider' => $menuDataProvider,
                'itemView' => '_touch-button',
                'summary' => false,
                'options' => [
                    'class' => 'b-touch-buttons row',
                ],
                'itemOptions' => [
                    'class' => 'b-touch-buttons__btn-wrap col-lg-3 col-sm-4 col-xs-6',
                ],
            ]) ?>
        </div>
        <div class="b-orders-column col-md-4 full-height overflow-auto">
            <?= ListView::widget([
                'dataProvider' => $ordersDataProvider,
                'itemView' => '_order-item',
                'emptyText' => 'Нет актуальных заказов.',
                'summary' => false,
            ]) ?>
        </div>
    </div>
</div>
<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Order $model
 */

use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;

?>
<div class="panel panel-primary b-order">
    <div class="panel-heading b-order__header">
        <strong class="b-order__label">Заказ №<?= $model->number ?></strong>
        <span class="b-order__time">(<?= $model->getCreateTime('H:i') ?>)</span>
        <?= Html::a(FA::icon('check'), ['/manager/complete-order', 'id' => $model->id], [
            'class' => 'text-white pull-right b-complete-mark b-order__complete-mark',
        ]) ?>
    </div>
    <ul class="list-group b-positions-list">
        <?php foreach ($model->positions as $position): ?>
            <li class="list-group-item list-group-item-info b-positions-list__position b-positions-list__position_info"><?= $position->positionData ?></li>
        <?php endforeach ?>
    </ul>
    <div class="panel-body">
        Итого: <?= Yii::$app->formatter->asCurrency($model->cost) ?>
    </div>
</div>

<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->view->params['section_name'] = 'Управление заказами';
        $this->view->params['home_url'] = ['/'];
        $this->view->params['header_menu'] = [
            ['label' => 'Панель менеджера', 'url' => ['/manager']],
            ['label' => 'Панель сборщика', 'url' => ['/collector']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

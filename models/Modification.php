<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%modification}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $product_id
 *
 * @property Product $product
 */
class Modification extends ActiveRecord implements ActionButtonInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%modification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'product_id'], 'required'],
            [['product_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'product_id' => 'Продукт',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     */
    public function getButtonName()
    {
        return $this->name;
    }

    /**
     * Возвращает URL для выбора модификации
     *
     * @return string
     */
    public function getActionUrl()
    {
        return Url::to(['/manager/select-modification', 'modification_id' => $this->id]);
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TradeOffer */

$this->title = 'Изменение торгового предложения: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->product->name), 'url' => ['/shop/product/view', 'id' => $model->product->id]];
$this->params['breadcrumbs'][] = ['label' => 'Торговые предложения', 'url' => ['/shop/trade-offer', 'product_id' => $model->product->id]];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->name), 'url' => ['/shop/trade-offer/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение предложения';
?>
<div class="trade-offer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

"use strict";

/**
 * Класс панели коллектора
 */
var Collector = (function () {
    function Collector() {
    }
    /**
     * Обработчик long poll
     */
    Collector.longPollHandler = function (data) {
        if (Collector.isFirstTimeEvent) {
            Collector.isFirstTimeEvent = false;
            return;
        }
        for (var eventKey in data)
            if (data.hasOwnProperty(eventKey)) {
                var $ordersList = Collector.getOrdersList(), eventValue = data[eventKey];
                if (eventKey == Collector.ORDER_CREATED) {
                    $ordersList.append(eventValue);
                }
                else if (eventKey == Collector.ORDER_DELETED) {
                    $ordersList.find('[data-order-id="' + eventValue + '"]').remove();
                }
            }
    };
    /**
     * Возвращает jQuery объект DOM-узла списка заказов
     *
     * @returns {{}}
     */
    Collector.getOrdersList = function () {
        return jQuery('.js-orders-list__list');
    };
    return Collector;
}());
/**
 * Флаг, определяющий, является ли событие первым в данной сессии
 *
 * @type {boolean}
 */
Collector.isFirstTimeEvent = true;
Collector.ORDER_CREATED = 'orderCreated';
Collector.ORDER_DELETED = 'orderDeleted';
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Modification */

$this->title = 'Изменение модификации: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->product->name), 'url' => ['/shop/product/view', 'id' => $model->product->id]];
$this->params['breadcrumbs'][] = ['label' => 'Модификации', 'url' => ['/shop/modification', 'product_id' => $model->product->id]];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->name), 'url' => ['/shop/modification/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение модификации';
?>
<div class="modification-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

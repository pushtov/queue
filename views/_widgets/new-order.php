<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\BaseDataProvider $dataProvider
 * @var string[] $renderedItems
 * @var string $emptyText
 * @var array $params
 */

use yii\bootstrap\Html;

$panelClass = isset($params['panelClass']) ? $params['panelClass'] : 'panel-default';
$isHeaderNeeded = isset($params['needHeader']) ? $params['needHeader'] : true;
$headerLegend = isset($params['headerLegend']) ? $params['headerLegend'] : 'Создание нового заказа';
?>
<div class="panel <?= $panelClass ?>">
    <?php if ($isHeaderNeeded): ?>
        <div class="panel-heading"><strong><?= $headerLegend ?></strong></div>
    <?php endif ?>
    <?php if ($dataProvider->count): ?>
    <ul class="list-group">
        <?= implode("\n", $renderedItems) ?>
    </ul>
    <?php endif ?>
    <div class="panel-body">
        <?php if ($dataProvider->count): ?>
            <?= Html::a('Завершить', ['/manager/create-order'], [
                'class' => 'btn btn-success',
            ]) ?>
            <?= Html::a('Отменить', ['/manager/cancel-order'], [
                'class' => 'btn btn-danger',
            ]); ?>
        <?php else: ?>
            <p><?= $emptyText ?></p>
        <?php endif ?>
    </div>
</div>
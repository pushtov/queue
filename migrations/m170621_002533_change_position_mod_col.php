<?php

use app\models\Position;
use yii\db\Migration;

class m170621_002533_change_position_mod_col extends Migration
{
    public function up()
    {
        $this->alterColumn(Position::tableName(), 'modification_id', $this->integer()->null());
    }

    public function down()
    {
        $this->alterColumn(Position::tableName(), 'modification_id', $this->integer()->notNull());
    }
}

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $product app\models\Product */
/* @var $model app\models\TradeOffer */

$this->title = 'Создание торгового предложения';
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($product->name), 'url' => ['/shop/product/view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = ['label' => 'Торговые предложения', 'url' => ['/shop/trade-offer', 'product_id' => $product->id]];
$this->params['breadcrumbs'][] = 'Новое предложение';
?>
<div class="trade-offer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\models\Category;
use app\models\Product;
use yii\db\Migration;

class m170625_200631_create_product_category_relation extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            Product::tableName(),
            'category_id',
            $this->integer()->notNull()
        );
        /** @var Category $category */
        $category = Category::find()->orderBy(['id' => SORT_ASC])->limit(1)->one();
        Yii::$app->db->createCommand()->update(Product::tableName(), ['category_id' => $category->id])->execute();
        $this->addForeignKey(
            'fk-product-category',
            Product::tableName(),
            'category_id',
            Category::tableName(),
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-product-category', Product::tableName());
        $this->dropColumn(Product::tableName(), 'category_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170625_200631_create_product_category_relation cannot be reverted.\n";

        return false;
    }
    */
}

<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Position $model
 * @var integer $key
 */

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;

?>
<li class="list-group-item list-group-item_new-order-position clearfix">
    <?= $model->positionData ?>
    <?= Html::a(FA::icon('trash'), ['/manager/delete-position', 'key' => $key], [
        'class' => 'btn btn-danger btn-sm pull-right',
        'title' => 'Удалить',
    ]) ?>
</li>

<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Order $model
 */

?>
<div class="panel panel-primary b-order b-order_big" data-order-id="<?= $model->id ?>">
    <div class="panel-heading b-order__header">
        <strong class="b-order__label">Заказ №<?= $model->number ?></strong>
        <span class="b-order__time">(<?= $model->getCreateTime('H:i') ?>)</span>
    </div>
    <ul class="list-group b-positions-list">
        <?php foreach ($model->positions as $position): ?>
            <li class="list-group-item list-group-item-info b-positions-list__position b-positions-list__position_info"><?= $position->getPositionData(false) ?></li>
        <?php endforeach ?>
    </ul>
</div>

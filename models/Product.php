<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 *
 * @property Modification[] $modifications
 * @property TradeOffer[] $tradeOffers
 * @property Category $category
 */
class Product extends ActiveRecord implements ActionButtonInterface
{
    /**
     * Имя класса, настоящего хозяина запрашиваемых данных
     * Костыль для вывода категорий рядом с продуктами в списке
     *
     * @var string
     */
    public $className;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_MIGRATION => ['name'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'category_id' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifications()
    {
        return $this->hasMany(Modification::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTradeOffers()
    {
        return $this->hasMany(TradeOffer::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     */
    public function getButtonName()
    {
        return $this->name;
    }

    /**
     * Возвращает URL добавления продукта в позицию
     * @return string
     * @throws ServerErrorHttpException
     */
    public function getActionUrl()
    {
        if ($this->className && $this->className != self::className()) {
            $class = new $this->className;
            if (!method_exists($class, 'getActionUrl')) {
                throw new ServerErrorHttpException('Некорректный альтернативный класс');
            }
            $class->id = $this->id;
            $class->name = $this->name;
            return call_user_func([$class, 'getActionUrl']);
        }
        return Url::to(['/manager/add-product', 'product_id' => $this->id]);
    }

    const SCENARIO_MIGRATION = 'migration';
}

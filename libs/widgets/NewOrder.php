<?php

namespace app\libs\widgets;

use yii\base\ErrorException;
use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Виджет для вывода позиций создаваемого заказа
 */
class NewOrder extends Widget
{
    /**
     * Провайдер данных
     *
     * @var \yii\data\BaseDataProvider
     */
    public $dataProvider;

    /**
     * Каталог с вьюхами виджетов
     *
     * @var string
     */
    public $widgetViewPath = '@app/views/_widgets';

    /**
     * Имя главной вьюхи виджета
     *
     * @var string
     */
    public $widgetView = 'new-order';

    /**
     * Имя вьюхи позиции виджета
     *
     * @var string
     */
    public $itemView = 'new-order-item';

    /**
     * Текст, выводимый при отсутствии результатов
     *
     * @var string
     */
    public $emptyText = 'Нет доступных элементов';

    /**
     * Дополнительные произвольные параметры для передачи во вьюшки
     *
     * @var array
     */
    public $params = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (!$this->dataProvider) {
            throw new ErrorException('Не установлен dataProvider');
        }
        $renderedItems = array_map(function ($i, ActiveRecord $model) {
            return $this->renderItem($model, ['key' => $i]);
        }, array_keys($this->dataProvider->models), $this->dataProvider->models);
        return $this->view->render($this->widgetViewPath . DIRECTORY_SEPARATOR . $this->widgetView, [
            'dataProvider' => $this->dataProvider,
            'renderedItems' => $renderedItems,
            'emptyText' => $this->emptyText,
            'params' => $this->params,
        ]);
    }

    /**
     * Отрисовывает позиции заказа
     *
     * @param ActiveRecord $model
     * @param array $params
     * @return string
     */
    private function renderItem(ActiveRecord $model, $params = [])
    {
        return $this->view->render($this->widgetViewPath . DIRECTORY_SEPARATOR . $this->itemView, ArrayHelper::merge([
            'model' => $model,
            'params' => $this->params,
        ], $params));
    }
}
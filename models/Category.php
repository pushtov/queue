<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property \app\models\ActionButtonInterface $children
 */
class Category extends ActiveRecord implements ActionButtonInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->getProducts()->select([
            'id',
            'name',
            'className' => new Expression('"' . addslashes(Product::className()) . '"'),
        ])->union($this->getCategories()->select([
            'id',
            'name',
            'className' => new Expression('"' . addslashes(Category::className()) . '"'),
        ]));
    }

    /**
     * Имя кнопки, выводимое в качестве текста ссылки
     *
     * @return string
     */
    public function getButtonName()
    {
        return Html::encode($this->name);
    }

    /**
     * Возвращает ссылку на экшн, выполняющий определённое действие
     *
     * @return string
     */
    public function getActionUrl()
    {
        return Url::to(['/manager', 'category_id' => $this->id]);
    }
}

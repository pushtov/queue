<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TradeOffer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->product->name), 'url' => ['/shop/product/view', 'id' => $model->product->id]];
$this->params['breadcrumbs'][] = ['label' => 'Торговые предложения', 'url' => ['/shop/trade-offer', 'product_id' => $model->product->id]];
$this->params['breadcrumbs'][] = Html::encode($model->name);
?>
<div class="trade-offer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить торговое предложение?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'price',
                'value' => Yii::$app->formatter->asCurrency($model->price),
            ],
            [
                'attribute' => 'product_id',
                'value' => Html::a($model->product->name, ['/shop/product/view', 'id' => $model->product->id]),
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>

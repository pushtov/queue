<?php

namespace app\controllers;

use app\libs\data\SessionDataProvider;
use app\libs\enums\OrderEvent;
use app\models\Category;
use app\models\Modification;
use app\models\Order;
use app\models\Position;
use app\models\Product;
use app\models\TradeOffer;
use izumi\longpoll\Event;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class ManagerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->view->params['section_name'] = 'Панель менеджера';
        $this->view->params['home_url'] = ['/manager'];
        $this->view->params['header_menu'] = [
            ['label' => 'Панель сборщика', 'url' => ['/collector']],
        ];
    }

    /**
     * Список продуктов и заказов
     *
     * @param bool $category_id
     * @return string
     */
    public function actionIndex($category_id = false)
    {
        $this->view->params['fullHeight'] = true;
        /** @var Category $category */
        if ($category_id === false) {
            $category = Category::find()->where(['parent_id' => null])->orderBy(['ID' => SORT_ASC])->limit(1)->one();
        } else {
            $category = Category::findOne($category_id);
            if ($category === null) {
                return $this->redirectWithError(['/manager'], 'Некорректный запрос');
            }
        }
        $menuDataProvider = new ActiveDataProvider(['query' => $category->getChildren()]);
        $newOrderDataProvider = new SessionDataProvider(['sessionKey' => 'positions']);
        $ordersDataProvider = new ActiveDataProvider([
            'query' => Order::find()->where(['status' => Order::STATUS_COMPLETED]),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);
        return $this->render('index', [
            'menuDataProvider' => $menuDataProvider,
            'newOrderDataProvider' => $newOrderDataProvider,
            'ordersDataProvider' => $ordersDataProvider,
        ]);
    }

    /**
     * Дообавляет продукт и при необходимости создаёт позицию
     *
     * @param integer|bool $product_id
     * @return \yii\web\Response
     */
    public function actionAddProduct($product_id = false)
    {
        $product = Product::findOne($product_id);
        if ($product === null) {
            return $this->redirectWithError(['/manager'], 'Произошла ошибка');
        }
        Yii::$app->session['new_position'] = new Position(['product_id' => $product->id]);
        return $this->redirect(['/manager/select-offer']);
    }

    /**
     * Устанавливает торговое предложение
     *
     * @param integer|bool $trade_offer_id
     * @return string|\yii\web\Response
     */
    public function actionSelectOffer($trade_offer_id = false)
    {
        if ($trade_offer_id !== false) {
            $tradeOffer = TradeOffer::findOne($trade_offer_id);
            if ($tradeOffer === null) {
                return $this->redirectWithError(['/manager'], 'Произошла ошибка');
            } else {
                Yii::$app->session['new_position']->trade_offer_id = $tradeOffer->id;
                return $this->redirect(['/manager/select-modification']);
            }
        }
        $tradeOffers = Yii::$app->session['new_position']->product->tradeOffers;
        if (count($tradeOffers) > 1 || count($tradeOffers) == 1 && $tradeOffers[0]->name != '-') {
            /** @var Position $position */
            $position = Yii::$app->session['new_position'];
            $dataProvider = new ActiveDataProvider(['query' => $position->product->getTradeOffers()]);
            return $this->render('select-trade-offer', [
                'dataProvider' => $dataProvider,
            ]);
        } elseif (count($tradeOffers) == 1) {
            Yii::$app->session['new_position']->trade_offer_id = $tradeOffers[0]->id;
            return $this->redirect(['/manager/select-modification']);
        } else {
            return $this->redirectWithError(['/manager'], 'Для продукта не созданы торговые предложения');
        }
    }

    /**
     * Устанавливает модификацию
     *
     * @param integer|bool $modification_id
     * @return string|\yii\web\Response
     */
    public function actionSelectModification($modification_id = false)
    {
        if ($modification_id !== false) {
            $modification = Modification::findOne($modification_id);
            if ($modification === null) {
                return $this->redirectWithError(['/manager'], 'Произошла ошибка');
            } else {
                Yii::$app->session['new_position']->modification_id = $modification->id;
                return $this->redirect(['/manager/set-count']);
            }
        }
        $modifications = Yii::$app->session['new_position']->product->modifications;
        if (empty($modifications)) {
            return $this->redirect(['/manager/set-count']);
        } else {
            /** @var Position $position */
            $position = Yii::$app->session['new_position'];
            $dataProvider = new ActiveDataProvider(['query' => $position->product->getModifications()]);
            return $this->render('select-modification', [
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Устанавливает количество для позиции
     *
     * @param integer|bool $count
     * @return string|\yii\web\Response
     */
    public function actionSetCount($count = false)
    {
        if ($count !== false) {
            Yii::$app->session['new_position']->count = $count;
            if (!isset(Yii::$app->session['positions'])) {
                Yii::$app->session['positions'] = [];
            }
            Yii::$app->session['positions'] = array_merge(Yii::$app->session['positions'], [Yii::$app->session['new_position']]);
            unset(Yii::$app->session['new_position']);
            return $this->redirect(['/manager']);
        }
        return $this->render('set-count');
    }

    /**
     * Создаёт заказ из выбранных позиций
     *
     * @return \yii\web\Response
     */
    public function actionCreateOrder()
    {
        /** @var Position[] $positions */
        if (!($positions = Yii::$app->session['positions'])) {
            return $this->redirectWithError(['/manager'], 'Не выбраны позиции');
        }
        $order = new Order([
            'cost' => array_sum(array_map(
                function ($price, $count) {
                    return $price * $count;
                },
                ArrayHelper::getColumn($positions, 'tradeOffer.price'),
                ArrayHelper::getColumn($positions, 'count')
            )),
            'status' => Order::STATUS_COMPLETED,
            'created_at' => time(),
        ]);
        $transaction = Yii::$app->db->beginTransaction();
        if (!$order->save()) {
            $transaction->rollBack();
            return $this->redirectWithError(['/manager'], 'Произошла ошибка при создании заказа');
        }
        $success = true;
        array_map(function (Position $position) use ($order, &$success) {
            $position->order_id = $order->id;
            $success = $success && $position->save();
        }, $positions);
        if (!$success) {
            $transaction->rollBack();
            return $this->redirectWithError(['/manager'], 'Произошла ошибка при сохранении позиций');
        }
        $transaction->commit();
        Order::clearNewOrderData();
        Event::triggerByKey(OrderEvent::ORDER_CREATED);
        return $this->redirectWithSuccess(['/manager'], "Заказ № {$order->id} успешно создан!");
    }

    /**
     * Отменяет создание нового заказа
     *
     * @return \yii\web\Response
     */
    public function actionCancelOrder()
    {
        Order::clearNewOrderData();
        return $this->redirectWithSuccess(['/manager'], 'Создание заказа отменено');
    }

    /**
     * Завершает работу над заказом
     *
     * @param integer $id Идентификатор заказа
     * @return \yii\web\Response
     */
    public function actionCompleteOrder($id)
    {
        /** @var Order $order */
        $order = Order::findOne(['id' => $id, 'status' => Order::STATUS_COMPLETED]);
        if ($order === null) {
            return $this->redirectWithError(['/manager'], 'Заказ не найден');
        }
        $order->status = Order::STATUS_DELETED;
        $order->completed_at = time();
        $order->save();
        Event::triggerByKey(OrderEvent::ORDER_DELETED);
        return $this->redirectWithSuccess(['/manager'], 'Заказ №' . $order->id . ' завершён!');
    }

    /**
     * Удаляет позицию из создаваемого заказа
     *
     * @param integer $key
     * @return \yii\web\Response
     */
    public function actionDeletePosition($key)
    {
        $positions = Yii::$app->session['positions'];
        if (!isset($positions[$key])) {
            return $this->redirectWithError(['/manager', 'Позиция не существует']);
        }
        unset($positions[$key]);
        Yii::$app->session['positions'] = array_values($positions);
        return $this->redirect(['/manager']);
    }

    /**
     * Возвращает редирект с установкой сообщения об ошибке
     *
     * @param array|string $url
     * @param string|bool $message
     * @param int $statusCode
     * @return \yii\web\Response
     */
    private function redirectWithError($url, $message = true, $statusCode = 302)
    {
        Yii::$app->session->setFlash('error', $message);
        return $this->redirect($url, $statusCode);
    }

    /**
     * Возвращает редирект с установкой сообщения об успешно выполненных действиях
     *
     * @param array|string $url
     * @param string|bool $message
     * @param int $statusCode
     * @return \yii\web\Response
     */
    private function redirectWithSuccess($url, $message = true, $statusCode = 302)
    {
        Yii::$app->session->setFlash('success', $message);
        return $this->redirect($url, $statusCode);
    }
}

<?php

namespace app\libs\enums;

use abhimanyu\enum\helpers\BaseEnum;
use app\models\Order;
use izumi\longpoll\Event;
use Yii;
use yii\web\ServerErrorHttpException;

/**
 * События для long poll, связанные с заказом
 *
 * @method static ORDER_CREATED()
 * @method static ORDER_DELETED()
 */
class OrderEvent extends BaseEnum
{
    const ORDER_CREATED = 'orderCreated';
    const ORDER_DELETED = 'orderDeleted';

    /**
     * Возвращает список событий для long polling.
     * Ключом массива являются коды событий.
     * Значениями являются функции, возвращающие данные для события.
     *
     * @return array
     */
    public function events()
    {
        return [
            static::ORDER_CREATED => [$this, 'getCreatedOrder'],
            static::ORDER_DELETED => [$this, 'getDeletedOrder'],
        ];
    }

    /**
     * Возвращает данные события для ответа long poll
     *
     * @param Event $event Long poll событие
     * @return mixed
     * @throws ServerErrorHttpException
     */
    public function getResult(Event $event)
    {
        if ($event->key !== $this->getValue()) {
            throw new ServerErrorHttpException('Передано некорректное событие');
        }
        $events = $this->events();
        return call_user_func($events[$event->key], $event);
    }

    /**
     * Возвращает html последнего созданного заказа
     *
     * @return string|null
     */
    public function getCreatedOrder()
    {
        $order = Order::lastCompleted();
        if ($order === null) {
            return null;
        }
        return Yii::$app->view->render('@app/views/collector/_order-item', [
            'model' => $order,
        ]);
    }

    /**
     * Возвращает id последнего удалённого заказа
     *
     * @return integer|null
     */
    public function  getDeletedOrder()
    {
        $order = Order::lastDeleted();
        if ($order === null) {
            return null;
        }
        return $order->id;
    }
}
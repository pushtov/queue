<?php
/**
 * @var $this yii\web\View
 * @var \app\models\Order[] $currentOrders
 */

use app\assets\CollectorAsset;
use app\libs\enums\OrderEvent;
use izumi\longpoll\widgets\LongPoll;
use yii\bootstrap\Html;
use yii\web\JsExpression;

$this->title = 'Панель сборщика';

CollectorAsset::register($this);

LongPoll::widget([
    'url' => ['polling'],
    'events' => OrderEvent::getConstantsByValue(),
    'callback' => new JsExpression('Collector.longPollHandler'),
]);
?>
<div class="b-collector-page">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="b-orders-list">
        <div class="b-orders-list__alerts js-orders-list__alerts"></div>
        <div class="b-orders-list__list js-orders-list__list">
            <?php if ($currentOrders) foreach ($currentOrders as $order): ?>
                <?= $this->render('@app/views/collector/_order-item', [
                    'model' => $order,
                ]) ?>
            <?php endforeach; ?>
        </div>
    </div><!-- .b-orders-list -->
</div>
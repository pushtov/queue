<?php
/**
 * @var \yii\web\View $this
 */

use yii\bootstrap\Html;

$this->title = 'Управление заказами';
?>
<div class="b-index-page">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="b-index-page__center-container row">
        <div class="b-touch-buttons__btn-wrap col-lg-3 col-sm-4 col-xs-6">
            <?= Html::a('Панель менеджера', ['/manager'], [
                'class' => 'b-touch-button b-index-page__touch-button',
            ]) ?>
        </div><!-- .b-touch-buttons__btn-wrap -->
        <div class="b-touch-buttons__btn-wrap col-lg-3 col-sm-4 col-xs-6">
            <?= Html::a('Панель сборщика', ['/collector'], [
                'class' => 'b-touch-button b-index-page__touch-button',
            ]) ?>
        </div><!-- .b-touch-buttons__btn-wrap -->
        <div class="b-touch-buttons__btn-wrap col-lg-3 col-sm-4 col-xs-6">
            <?= Html::a('Управление ассортиментом', ['/shop'], [
                'class' => 'b-touch-button b-index-page__touch-button',
            ]) ?>
        </div><!-- .b-touch-buttons__btn-wrap -->
    </div><!-- .b-index-page__center-container -->
</div><!-- .b-index-page -->

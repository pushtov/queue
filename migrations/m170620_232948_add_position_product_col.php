<?php

use app\models\Position;
use app\models\Product;
use yii\db\Migration;

class m170620_232948_add_position_product_col extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            Position::tableName(),
            'product_id',
            $this->integer()->notNull()->after('order_id')
        );
        $this->addForeignKey(
            'fk-position-product',
            Position::tableName(),
            'product_id',
            Product::tableName(),
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-position-product', Position::tableName());
        $this->dropColumn(Position::tableName(), 'product_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170620_232948_add_position_product_col cannot be reverted.\n";

        return false;
    }
    */
}

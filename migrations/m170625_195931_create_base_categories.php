<?php

use app\models\Category;
use yii\db\Migration;

class m170625_195931_create_base_categories extends Migration
{
    public function safeUp()
    {
        $rootCategory = new Category();
        $rootCategory->name = 'Главная';
        $rootCategory->save();
        $otherCategory = new Category();
        $otherCategory->name = 'Остальное';
        $otherCategory->parent_id = $rootCategory->id;
        $otherCategory->save();
    }

    public function safeDown()
    {
        $categories = Category::find()->where(['parent_id' => null])->all();
        foreach ($categories as $category) {
            $category->delete();
        }
    }
}

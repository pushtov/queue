<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%position}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $trade_offer_id
 * @property integer $modification_id
 * @property integer $count
 *
 * @property string $positionData
 *
 * @property Modification $modification
 * @property Order $order
 * @property Product $product
 * @property TradeOffer $tradeOffer
 */
class Position extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'trade_offer_id'], 'required'],
            [['order_id', 'product_id', 'trade_offer_id', 'modification_id', 'count'], 'integer'],
            [['modification_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modification::className(), 'targetAttribute' => ['modification_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['trade_offer_id'], 'exist', 'skipOnError' => true, 'targetClass' => TradeOffer::className(), 'targetAttribute' => ['trade_offer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'trade_offer_id' => 'Trade Offer ID',
            'modification_id' => 'Modification ID',
            'count' => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModification()
    {
        return $this->hasOne(Modification::className(), ['id' => 'modification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTradeOffer()
    {
        return $this->hasOne(TradeOffer::className(), ['id' => 'trade_offer_id']);
    }

    /**
     * Возвращает данные о позиции
     *
     * @param bool $withPrice
     * @return string
     */
    public function getPositionData($withPrice = true)
    {
        $result = [];
        $result[] = $this->product->name;
        if ($this->modification !== null) {
            $result[] = $this->modification->name;
        }
        if ($this->tradeOffer !== null) {
            if ($this->tradeOffer->name != '-') {
                $offer = $this->tradeOffer->name;
                if ($withPrice) {
                    $offer .= ' (' . Yii::$app->formatter->asCurrency($this->tradeOffer->price) . ')';
                }
                $result[] = $offer;
            } elseif($withPrice) {
                $result[] = Yii::$app->formatter->asCurrency($this->tradeOffer->price);
            }
        }
        if ($this->count) {
            $result[] = $this->count . ' шт.';
        }
        return implode(' / ', $result);
    }
}

/**
 *
 */
var Counter = (function () {
    /**
     * Кноструктор объекта
     *
     * @constructor
     */
    function Counter() {
        var _this = this;
        this.wrapper = $('.js-count-input');
        this.input = this.wrapper.find('.js-count-input__input');
        this.form = this.input.parents('form');
        this.backspace = this.wrapper.find('.js-count-input__button_backspace');
        this.buttons = {};
        var buttons = this.wrapper.find('.js-count-input__button');
        if (!this.wrapper.length || !this.input.length || !buttons.length) {
            throw 'Созданы не все элементы управления';
        }
        buttons.each(function () {
            var $this = $(this);
            var button = new CounterButton($this, _this);
            _this.buttons[button.value] = button;
        });
    }

    /**
     * Инициализирует объект
     */
    Counter.prototype.init = function () {
        var _this = this;
        this.input.on('change input', inputHandler);
        this.backspace.on('click', function (e) {
            e.preventDefault();
            backspace(_this.input);
        });
        this.form.on('submit', function () {
            var MIN_VALUE = 1,
                value = parseInt(_this.input.val());
            if (isNaN(value) || value < MIN_VALUE) {
                _this.input.focus().val('');
                return false;
            }
        })
    };

    /**
     * Обработчик ввода данных в поле input
     */
    function inputHandler() {
        var MIN_VALUE = 1;
        var $this = $(this),
            value = $this.val(),
            intValue = parseInt(value);
        if (value == '') {
            return;
        }
        if (isNaN(intValue) || intValue < MIN_VALUE) {
            $this.val('');
        }
    }

    /**
     * Удаляет последний символ в поле ввода
     *
     * @param $input
     */
    function backspace($input) {
        var val = $input.val();
        if (val.length) {
            val = val.substr(0, -1);
        }
        $input.val(val);
        $input.focus();
    }

    return Counter;
}());

/**
 *
 */
var CounterButton = (function () {

    /**
     * Конструктор объекта
     *
     * @param {jQuery} node jQuery объект кнопки
     * @param {Counter} counter
     */
    function CounterButton(node, counter) {
        this.node = node;
        this.value = parseInt(node.attr('data-value'));
        this.counter = counter;
        var _this = this;
        this.node.on('click', function (e) {
            e.preventDefault();
            return _this.clickHandler();
        });
    }

    /**
     * Обработчик клика
     */
    CounterButton.prototype.clickHandler = function () {
        var input = this.counter.input;
        input.val(input.val() + this.value.toString());
        input.trigger('change').focus();
    };

    /**
     *
     * @returns {number}
     */
    CounterButton.prototype.getValue = function () {
        return value;
    };

    return CounterButton;
}());

// Запуск скрипта
(new Counter()).init();
<?php

namespace app\libs\data;

use Yii;
use yii\base\InvalidValueException;
use yii\data\BaseDataProvider;
use yii\helpers\ArrayHelper;

class SessionDataProvider extends BaseDataProvider
{
    /**
     * Ключ, который будет использован для поиска значений в массиве сессии.
     * Если необходимо передать путь к значению, можно воспользоваться следующими способами:
     * - Перечислить ключи в строке, разделяя их точками: key1.key2...keyN
     * - Передать массив ключей: ['key1', 'key2', ..., 'keyN']
     *
     * @var string|array
     */
    public $sessionKey;

    public function init()
    {
        parent::init();
        if ($this->sessionKey === null) {
            throw new InvalidValueException('Не указан ключ сессии');
        }
        if (!is_array($this->sessionKey)) {
            $this->sessionKey = array_map('trim', explode('.', $this->sessionKey));
        }
    }

    /**
     * Prepares the data models that will be made available in the current page.
     * @return array the available data models
     */
    protected function prepareModels()
    {
        $keys = $this->sessionKey;
        $firstKey = array_shift($keys);
        $models = Yii::$app->session[$firstKey] ?? [];
        if (empty($models) || empty($keys)) {
            return $models;
        }
        return ArrayHelper::getValue($models, $this->sessionKey, []);
    }

    /**
     * Prepares the keys associated with the currently available data models.
     * @param array $models the available data models
     * @return array the keys
     */
    protected function prepareKeys($models)
    {
        return array_keys($models);
    }

    /**
     * Returns a value indicating the total number of data models in this data provider.
     * @return int total number of data models in this data provider.
     */
    protected function prepareTotalCount()
    {
        return count($this->prepareModels());
    }
}
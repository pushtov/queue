<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $product \app\models\Product */
/* @var $model app\models\Modification */

$this->title = 'Создание модификации';
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($product->name), 'url' => ['/shop/product/view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = ['label' => 'Модификации', 'url' => ['/shop/modification', 'product_id' => $product->id]];
$this->params['breadcrumbs'][] = 'Новая модификация';
?>
<div class="modification-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\models\Modification;
use app\models\Product;
use app\models\TradeOffer;
use yii\db\Migration;

class m170620_201034_init_product_data extends Migration
{
    public function safeUp()
    {
        $this->createMilkCocktail();
        $this->createIceCream();
        $this->createOxygenCocktail();
        $this->createCoffee();
        $this->createCarbonatedDrinks();
        $this->createOthers();
    }

    public function safeDown()
    {
        $products = Product::find()->all();
        if (!is_null($products)) {
            array_map(function (Product $product) {
                $product->delete();
            }, $products);
        }
    }

    /**
     * Создаёт продукт Молочные коктейли
     */
    private function createMilkCocktail()
    {
        // Продукт
        $product = new Product(['name' => 'Молочные коктейли']);
        $product->scenario = Product::SCENARIO_MIGRATION;
        $product->save();

        // Торговые предложения
        (new TradeOffer([
            'name' => '500 мл',
            'price' => 100,
            'product_id' => $product->id,
        ]))->save();
        (new TradeOffer([
            'name' => '300 мл',
            'price' => 70,
            'product_id' => $product->id,
        ]))->save();

        // Модификации
        (new Modification(['name' => 'Малина', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Ананас', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Карамель', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Груша', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Виноград', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Дыня', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Ваниль', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Вишня', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Фисташка', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Яблоко', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Черная смородина', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Кокос', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Клубника', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Яблоко', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Шоколад', 'product_id' => $product->id]))->save();
    }

    /**
     * Создаёт продукт Мороженое
     */
    private function createIceCream()
    {
        // Продукт
        $product = new Product(['name' => 'Мороженое']);
        $product->scenario = Product::SCENARIO_MIGRATION;
        $product->save();

        // Торговые предложения
        (new TradeOffer([
            'name' => 'Большое',
            'price' => 100,
            'product_id' => $product->id,
        ]))->save();
        (new TradeOffer([
            'name' => 'Маленькое',
            'price' => 70,
            'product_id' => $product->id,
        ]))->save();
        (new TradeOffer([
            'name' => 'Креманка',
            'price' => 150,
            'product_id' => $product->id,
        ]))->save();

        // Модификации
        (new Modification(['name' => 'Карамель', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Клубника', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Шоколад', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Сливочное', 'product_id' => $product->id]))->save();
    }

    /**
     * Создаёт продукт Кислородные коктейли
     */
    private function createOxygenCocktail()
    {
        // Продукт
        $product = new Product(['name' => 'Кислородные коктейли']);
        $product->scenario = Product::SCENARIO_MIGRATION;
        $product->save();

        // Торговые предложения
        (new TradeOffer([
            'name' => '500 мл',
            'price' => 100,
            'product_id' => $product->id,
        ]))->save();

        // Модификации
        (new Modification(['name' => 'Клубника', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Виноград', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Яблоко', 'product_id' => $product->id]))->save();
    }

    /**
     * Создаёт продукт Кофе
     */
    private function createCoffee()
    {
        // Продукт
        $product = new Product(['name' => 'Кофе']);
        $product->scenario = Product::SCENARIO_MIGRATION;
        $product->save();

        // Торговые предложения
        (new TradeOffer([
            'name' => '300 мл',
            'price' => 120,
            'product_id' => $product->id,
        ]))->save();
        (new TradeOffer([
            'name' => '200 мл',
            'price' => 100,
            'product_id' => $product->id,
        ]))->save();

        // Модификации
        (new Modification(['name' => 'Капучино', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Американо', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Горячий шоколад', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Лате', 'product_id' => $product->id]))->save();
    }

    /**
     * Создаёт продукт Газированные напитки
     */
    private function createCarbonatedDrinks()
    {
        // Продукт
        $product = new Product(['name' => 'Газированные напитки']);
        $product->scenario = Product::SCENARIO_MIGRATION;
        $product->save();

        // Торговые предложения
        (new TradeOffer([
            'name' => '500 мл',
            'price' => 100,
            'product_id' => $product->id,
        ]))->save();
        (new TradeOffer([
            'name' => '300 мл',
            'price' => 70,
            'product_id' => $product->id,
        ]))->save();

        // Модификации
        (new Modification(['name' => 'Пепси', 'product_id' => $product->id]))->save();
        (new Modification(['name' => '7UP', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Миринда', 'product_id' => $product->id]))->save();
        (new Modification(['name' => 'Маунтин Дью', 'product_id' => $product->id]))->save();
    }

    /**
     * Прочие продукты
     */
    private function createOthers()
    {
        // Сахарная вата
        $product = new Product(['name' => 'Сахарная вата']);
        $product->scenario = Product::SCENARIO_MIGRATION;
        $product->save();
        (new TradeOffer(['name' => '-', 'price' => 100, 'product_id' => $product->id]))->save();

        // Свежевыжатые соки
        $product = new Product(['name' => 'Свежевыжатые соки']);
        $product->scenario = Product::SCENARIO_MIGRATION;
        $product->save();
        (new TradeOffer(['name' => '-', 'price' => 200, 'product_id' => $product->id]))->save();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170620_201034_init_product_data cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use app\models\Order;
use yii\db\Migration;

class m170622_182049_add_order_completed_at_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(
            Order::tableName(),
            'completed_at',
            $this->integer()
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Order::tableName(), 'completed_at');
    }
}

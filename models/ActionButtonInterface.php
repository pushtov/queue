<?php

namespace app\models;

/**
 * @property string $buttonName
 * @property string $actionUrl
 */
interface ActionButtonInterface
{
    /**
     * Имя кнопки, выводимое в качестве текста ссылки
     *
     * @return string
     */
    public function getButtonName();

    /**
     * Возвращает ссылку на экшн, выполняющий определённое действие
     *
     * @return string
     */
    public function getActionUrl();
}
<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m170625_195407_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'parent_id' => $this->integer(),
        ]);
        $this->addForeignKey(
            'fk-category-category',
            '{{%category}}',
            'parent_id',
            '{{%category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-category-category', '{{%category}}');
        $this->dropTable('{{%category}}');
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = 'Изменение продукта: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/shop/product']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->name), 'url' => ['/shop/product/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение продукта';
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

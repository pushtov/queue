<?php

namespace app\assets;

use yii\web\AssetBundle;

class CountAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/set-count.js?v1',
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
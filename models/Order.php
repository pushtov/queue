<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property string $cost
 * @property integer $status
 * @property integer $created_at
 * @property integer $completed_at
 *
 * @property integer $number
 * @property string $createTime
 * @property string $completeTime
 *
 * @property Position[] $positions
 */
class Order extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cost', 'status', 'created_at'], 'required'],
            [['cost'], 'number'],
            [['status', 'created_at', 'completed_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cost' => 'Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'completed_at' => 'Completed At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(), ['order_id' => 'id']);
    }

    /**
     * Возвращает номер заказа (номер заказа может быть не более 100, затем обнуляться)
     *
     * @return int
     */
    public function getNumber()
    {
        $remainder = $this->id % 100;
        if ($remainder == 0) {
            $remainder = 100;
        }
        return $remainder;
    }

    /**
     * Возвращает отформатированную дату создания зааза
     *
     * @param string $format
     * @return false|string
     */
    public function getCreateTime($format = 'd.m.Y H:i')
    {
        return date($format, $this->created_at);
    }

    /**
     * Возвращает отформатированную дату завершения заказа
     *
     * @param string $format
     * @return false|string
     */
    public function getCompleteTime($format = 'd.m.Y H:i:s')
    {
        return date($format, $this->completed_at);
    }

    /**
     * Возвращает последний активный заказ
     *
     * @return Order|array
     */
    public static function lastCompleted()
    {
        return static::find()
            ->where(['status' => static::STATUS_COMPLETED])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->one();
    }

    /**
     * Возвращает последний удалённый заказ
     *
     * @return Order|array
     */
    public static function lastDeleted()
    {
        return static::find()
            ->where(['status' => static::STATUS_DELETED])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->one();
    }

    /**
     * Удаляет из сессии все данные о создающемся заказе
     */
    public static function clearNewOrderData()
    {
        unset(Yii::$app->session['positions'], Yii::$app->session['new_position']);
    }

    const STATUS_BLANK = 10;
    const STATUS_COMPLETED = 20;
    const STATUS_DELETED = 30;
}

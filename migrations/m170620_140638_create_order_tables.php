<?php

use yii\db\Migration;

class m170620_140638_create_order_tables extends Migration
{
    public function safeUp()
    {
        // Таблица заказов
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'cost' => $this->decimal(10, 2)->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        // Таблица позиций
        $this->createTable('{{%position}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'trade_offer_id' => $this->integer()->notNull(),
            'modification_id' => $this->integer()->notNull(),
            'count' => $this->integer()->notNull()->defaultValue(1),
        ]);
        $this->addForeignKey(
            'fk-position-order',
            '{{%position}}',
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-position-trade_offer',
            '{{%position}}',
            'trade_offer_id',
            '{{%trade_offer}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-position-modification',
            '{{%position}}',
            'modification_id',
            '{{%modification}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-position-modification', '{{%position}}');
        $this->dropForeignKey('fk-position-trade_offer', '{{%position}}');
        $this->dropForeignKey('fk-position-order', '{{%position}}');
        $this->dropTable('{{%position}}');
        $this->dropTable('{{%order}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170620_140638_create_order_tables cannot be reverted.\n";

        return false;
    }
    */
}

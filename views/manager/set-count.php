<?php
/**
 * @var $this yii\web\View
 */

use app\assets\CountAsset;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;

$this->title = 'Количество';

CountAsset::register($this);
?>
<div class="b-select-trade-offer">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::beginForm('', 'get') ?>
    <div class="b-count-input js-count-input row">
        <div class="b-count-input__inner col-xs-12 col-md-6">
            <div class="form-group">
                <?= Html::input(
                    'number',
                    'count',
                    null,
                    [
                        'id' => 'number-input',
                        'class' => 'b-count-input__input js-count-input__input form-control',
                        'placeholder' => 'Введите число',
                        'autofocus' => true,
                    ]
                ) ?>
            </div><!-- .input-group -->
            <div class="b-count-input__button-group">
                <?= Html::a('1', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 1,
                ]) ?>
                <?= Html::a('2', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 2,
                ]) ?>
                <?= Html::a('3', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 3,
                ]) ?>
                <?= Html::a('4', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 4,
                ]) ?>
                <?= Html::a('5', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 5,
                ]) ?>
                <?= Html::a('6', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 6,
                ]) ?>
                <?= Html::a('7', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 7,
                ]) ?>
                <?= Html::a('8', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 8,
                ]) ?>
                <?= Html::a('9', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 9,
                ]) ?>
                <?= Html::button(FA::icon('check'), [
                    'type' => 'submit',
                    'class' => 'b-count-input__button b-count-input__button_success',
                ]) ?>
                <?= Html::a('0', '#number-input', [
                    'class' => 'b-count-input__button js-count-input__button',
                    'data-value' => 0,
                ]) ?>
                <?= Html::a(FA::icon('chevron-left'), '#', [
                    'class' => 'b-count-input__button js-count-input__button_backspace',
                    'title' => 'Стереть'
                ]) ?>
            </div><!-- .b-count-input__button-group -->
        </div><!-- .b-count-input__inner -->
    </div><!-- .b-count-input -->
    <?= Html::endForm() ?>
</div>
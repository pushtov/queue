<?php

use app\models\Order;
use yii\db\Migration;

class m170620_211046_add_order_status_col extends Migration
{
    public function up()
    {
        $this->addColumn(
            Order::tableName(),
            'status',
            $this
                ->integer()
                ->notNull()
                ->defaultValue(Order::STATUS_COMPLETED)
                ->after('cost')
        );
    }

    public function down()
    {
        $this->dropColumn(Order::tableName(), 'status');
    }
}
